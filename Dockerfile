# build stage
FROM node:14 AS build

WORKDIR /usr/src/app

# all dependencies needed for building
COPY package*.json .
RUN npm clean-install

COPY . .env.production.local .
RUN npm run clean && npm run build

# production stage
FROM node:14-alpine3.13
# FROM node:14

# install [Tini](https://github.com/krallin/tini)
RUN apk add --no-cache tini
ENTRYPOINT ["/sbin/tini", "--"]

EXPOSE 3000/tcp
WORKDIR /usr/src/app
VOLUME ["/usr/src/app/db"]

# only production dependencies needed here
COPY package*.json .
RUN npm clean-install --only=production

COPY --from=build /usr/src/app/.next .next
COPY --from=build /usr/src/app/.env.production.local .env.production.local
CMD ["npm", "start"]

# Listmate

> Shopping list designed with roommates in mind

## Getting Started

1. Create environment file and update it:

   ```bash
   cp .env.example.local .env.development.local
   ```

2. Install dependencies and run the development server:

   ```bash
   npm install
   npm run dev
   ```

3. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Production

1. Create environment file and update it:

   ```bash
   cp .env.example.local .env.production.local
   ```

2. Build and start the application:

   ```bash
   npm run build
   npm start
   ```

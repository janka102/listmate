import "tailwindcss/tailwind.css";
import type { AppProps } from "next/app";
import Head from "next/head";

import Container from "../components/Container";
import Footer from "../components/Footer";
import NavBar from "../components/NavBar";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div className="flex flex-col h-screen">
      <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NavBar />

      <div className="flex flex-col flex-1 overflow-y-auto">
        <main className="flex flex-1 bg-gray-900 text-gray-50">
          <Container>
            <div className="my-4">
              <Component {...pageProps} />
            </div>
          </Container>
        </main>

        <Footer />
      </div>
    </div>
  );
}
export default MyApp;

import Link from "next/link";
import Head from "next/head";
import useSWR from "swr";
import { PlusIcon } from "@heroicons/react/solid";
import { useState } from "react";

import type { ListItem } from "../lib/db";
import CheckBox from "../components/CheckBox";
import List from "../components/List";
import Interval from "../components/Interval";
import TagList from "../components/TagList";

import { withPageSession } from "../lib/session";

const head = (
  <Head>
    <title>Listmate</title>
  </Head>
);

type NewListItem = {
  name: string;
  done: boolean;
  tags: string[];
  modified: string;
};

function relTime(date: Date) {
  const now = Date.now();
  let diff = (now - date.getTime()) / 1000;

  if (diff < 60) {
    return "just\nnow";
    // return `${diff | 0}s\nago`;
  }

  diff /= 60;

  if (diff < 60) {
    return `${diff | 0}m\nago`;
  }

  diff /= 60;

  if (diff < 24) {
    return `${diff | 0}h\nago`;
  }

  diff /= 24;

  if (diff < 28) {
    return `${diff | 0}d\nago`;
  }

  diff /= 7;

  return `${diff | 0}w\nago`;
}

function Item({ item, update }: { item: ListItem; update: Function }) {
  const [ago, setAgo] = useState("");
  const modified = new Date(item.modified);

  const backgroundOpacity = item.done ? "bg-opacity-25" : "bg-opacity-0";

  return (
    <div className="flex">
      <Link href={`/items/${item.id}`}>
        <a
          className={
            "flex flex-1 pl-1 bg-gray-700 focus:bg-opacity-50 hover:bg-opacity-50 " +
            backgroundOpacity
          }
        >
          <span className="flex flex-col flex-1 justify-center py-1">
            <div className="leading-tight mb-1">{item.name}</div>
            <TagList tags={item.tags} readonly={true} />
          </span>
          <Interval
            onTick={() => setAgo(relTime(modified))}
            ms={5000}
            delay={
              (modified.getSeconds() % 5) * 1000 + modified.getMilliseconds()
            }
          >
            <span
              className="flex items-center text-center whitespace-pre text-sm px-2 py-1 leading-tight"
              title={item.modified}
            >
              {ago}
            </span>
          </Interval>
        </a>
      </Link>
      <CheckBox
        size={7}
        className="px-2 py-1"
        checked={item.done}
        onChange={(done) =>
          update({ ...item, done, modified: new Date().toJSON() })
        }
      />
    </div>
  );
}

export default function Home() {
  const { data, error, mutate } = useSWR<ListItem[]>("/api/items");
  const [filterText, setFilterText] = useState("");
  const [filterTags, setFilterTags] = useState([] as string[]);

  if (error) {
    return (
      <div>
        {head}Error: {error}
      </div>
    );
  }

  if (!data) {
    return <div>{head}Loading...</div>;
  }

  const allTags = Array.from(
    new Set(
      data.reduce((allTags, item) => allTags.concat(item.tags), [] as string[])
    )
  );
  const availableTags = allTags.filter((tag) => !filterTags.includes(tag));
  const items = data
    .filter(
      (item) =>
        item.name.toLowerCase().includes(filterText.toLowerCase()) &&
        filterTags.every((tag) => item.tags.includes(tag))
    )
    .sort(
      (a, b) => Number(a.modified > b.modified)
      // Number(a.done !== b.done ? a.done > b.done : a.modified > b.modified)
    );
  const todoItems = items.filter(({ done }) => !done);
  const completedItems = items.filter(({ done }) => done);

  const update = async (newItem: ListItem | NewListItem) => {
    const newItems = [...items];
    let apiEndpoint = "/api/items";

    if ("id" in newItem) {
      newItems.splice(
        newItems.findIndex(({ id }) => id === (newItem as ListItem).id),
        1
      );
      apiEndpoint += `/${newItem.id}`;
    } else {
      newItem = {
        ...newItem,
        id: "xxxx-xxxx".replace(/x/g, () =>
          ((Math.random() * 16) | 0).toString(16)
        ),
      };
    }

    newItems.push(newItem);

    // preemptive local update
    mutate(newItems, false);

    await fetch(apiEndpoint, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(newItem),
    });
    mutate();
  };

  return (
    <div className="flex flex-col max-w-lg mx-auto">
      {head}

      <div className="flex justify-center">
        <div className="flex flex-col justify-center flex-1 mr-2">
          <input
            type="text"
            className="py-1 px-2 mb-2 max-w-full bg-gray-800 border-b-2 border-gray-900 focus:border-green-600 outline-none"
            placeholder="Search or Add"
            value={filterText}
            onChange={(e) => setFilterText(e.target.value)}
          />
          <div className="flex">
            Tags:
            <TagList
              tags={filterTags}
              autocomplete={availableTags}
              onChange={(tags) => setFilterTags(tags)}
              className="inline-block ml-1 flex-1"
            />
          </div>
        </div>
        <button
          className="px-2 py-1 bg-green-800 hover:bg-green-700 focus:bg-green-700"
          onClick={async () => {
            const newItem = {
              name: filterText,
              done: false,
              tags: filterTags,

              modified: new Date().toJSON(),
            };

            setFilterText("");
            setFilterTags([]);

            update(newItem);
          }}
        >
          <PlusIcon className="h-7 w-7" />
        </button>
      </div>
      <hr className="my-2 border-gray-500" />
      <List>
        {todoItems.map((item) => (
          <List.Item key={item.id}>
            <Item item={item} update={update} />
          </List.Item>
        ))}
      </List>
      {todoItems.length && completedItems.length ? (
        <hr className="my-2 border-gray-500" />
      ) : null}
      <List>
        {completedItems.map((item) => (
          <List.Item key={item.id}>
            <Item item={item} update={update} />
          </List.Item>
        ))}
      </List>
    </div>
  );
}

export const getServerSideProps = withPageSession((context) => {
  const { session } = context;

  return {
    props: { session },
  };
});

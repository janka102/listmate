import type { NextApiResponse } from "next";

import { withApiSession } from "../../../lib/session";
import db, { Data, ListItem } from "../../../lib/db";

export default withApiSession((req, res: NextApiResponse<Data | ListItem>) => {
  let { id } = req.query;

  if (Array.isArray(id)) {
    id = id[0];
  }

  const item = db.data.get("items").find({ id }).value();

  if (!item) {
    res.status(404);
    res.end();
    return;
  }

  if (req.method === "POST") {
    const { name, done, tags } = req.body;

    if (done !== item.done) {
      item.modified = new Date().toJSON();
    }

    Object.assign(item, { name, done, tags });
    db.write();

    res.status(200).json(item);
    return;
  }

  if (req.method === "DELETE") {
    db.data.get("items").remove({ id }).value();
    db.write();

    res.status(200).end();
    return;
  }

  res.status(200).json(item);
});

import type { NextApiResponse } from "next";

import { withApiSession } from "../../../lib/session";
import db, { ListItem } from "../../../lib/db";
import uuid from "../../../lib/uuid";

export default withApiSession(
  (req, res: NextApiResponse<ListItem | ListItem[]>) => {
    if (req.method === "POST") {
      const { name, done, tags } = req.body;
      const id = uuid();
      const item = { id, name, done, tags, modified: new Date().toJSON() };

      db.data.get("items").value().push(item);
      db.write();

      res.status(200).json(item);
      return;
    }

    res.status(200).json(db.data.get("items").value());
  }
);

import type { NextApiResponse } from "next";

import { withApiSession } from "../../lib/session";
import db from "../../lib/db";

export default withApiSession((req, res: NextApiResponse<string[]>) => {
  res
    .status(200)
    .json(db.data.get("items").map("tags").flatMap().uniq().value());
});

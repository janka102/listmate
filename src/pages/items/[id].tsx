import Head from "next/head";
import { useRouter } from "next/router";
import useSWR from "swr";
import { TrashIcon } from "@heroicons/react/solid";
import { useCallback } from "react";
import { debounce } from "lodash";

import type { ListItem } from "../../lib/db";
import CheckBox from "../../components/CheckBox";
import TagList from "../../components/TagList";

import { withPageSession } from "../../lib/session";

export default function ItemPage({}) {
  const router = useRouter();
  const { id } = router.query;
  const { data, error, mutate } = useSWR<ListItem>(
    id ? "/api/items/" + id : null
  );
  const { data: allTags, error: tagsError } = useSWR<string[]>("/api/tags");
  const handleUpdate = useCallback(
    debounce(async (updatedData) => {
      mutate(
        await fetch("/api/items/" + updatedData.id, {
          method: "POST",
          headers: {
            "content-type": "application/json",
          },
          body: JSON.stringify(updatedData),
        }).then((res) => res.json()),
        false
      );
    }, 300),
    []
  );

  if (error || tagsError) {
    return (
      <div>
        <Head>
          <title>Listmate</title>
        </Head>
        Error: {error}
      </div>
    );
  }

  if (!data || !allTags) {
    return (
      <div>
        <Head>
          <title>Listmate</title>
        </Head>
        Loading...
      </div>
    );
  }

  const update = async (newData: Partial<ListItem>) => {
    const updatedData = Object.assign({}, data, newData);
    mutate(updatedData, false);

    handleUpdate(updatedData);
  };

  return (
    <div className="flex flex-col max-w-xl mx-auto">
      <Head>
        <title>Listmate | {data.name}</title>
      </Head>

      <div className="flex flex-col justify-center space-y-4">
        <label>
          <div className="mb-1">Name</div>
          <input
            type="text"
            className="px-2 py-1 -mb-px w-full bg-gray-800 border-b-2 border-gray-900 focus:border-green-600 outline-none"
            placeholder="Search or Add"
            value={data.name}
            onChange={(e) => update({ name: e.target.value })}
          />
        </label>
        <div>
          <div className="mb-1">Tags</div>
          <TagList
            tags={data.tags}
            autocomplete={allTags}
            onChange={(tags) => update({ tags })}
          />
        </div>

        <label className="flex mr-auto">
          Done
          <CheckBox
            size={5}
            className="p-0.5 ml-2"
            checked={data.done}
            onChange={(done) => update({ done })}
          />
        </label>

        <div>
          <div className="mb-1">Modified</div>
          <div>
            {new Date(data.modified).toLocaleString("en-US", {
              timeZoneName: "short",
            })}
          </div>
        </div>

        <div className="flex mr-auto">
          Delete
          <button
            className="p-0.5 ml-2 bg-red-700 hover:bg-red-600 focus:bg-red-600"
            onClick={async () => {
              await fetch("/api/items/" + data.id, {
                method: "DELETE",
              });

              router.push("/");
            }}
          >
            <TrashIcon className="w-5 h-5" />
          </button>
        </div>
      </div>
    </div>
  );
}

export const getServerSideProps = withPageSession((context) => {
  const { session } = context;

  return {
    props: { session },
  };
});

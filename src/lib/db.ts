import fs from "fs";
import path from "path";
import lodash, { LoDashStatic, ObjectChain } from "lodash";

const __dirname = path.dirname(new URL(import.meta.url).pathname);
const DB_DIR = path.resolve(__dirname, "..", "..", "db");
const DB_NAME = "listmate.json";
const DB_PATH = path.join(DB_DIR, DB_NAME);

if (!fs.existsSync(DB_DIR)) {
  fs.mkdirSync(DB_DIR);
}

export type ListItem = {
  id: string;
  name: string;
  done: boolean;
  tags: string[];
  modified: string;
};

export type Data = {
  items: ListItem[];
};

class LodashJsonStorage<T extends Object> {
  filename: string;
  tempFilename: string;
  lodash: LoDashStatic;
  data: ObjectChain<T>;

  constructor(filename: string, initialValue: T) {
    this.filename = filename;
    this.tempFilename = path.join(
      path.dirname(filename),
      `.${path.basename(filename)}.tmp`
    );
    this.lodash = lodash.runInContext();

    this.data = this._read(initialValue);
    this.write();
  }

  _read(initialValue: T) {
    let data: ObjectChain<T>;
    try {
      data = this.lodash.chain(
        JSON.parse(fs.readFileSync(this.filename, "utf-8")) as T
      );
      data.defaults(initialValue).value();
    } catch (e) {
      if (e.code !== "ENOENT") {
        throw e;
      }

      data = this.lodash.chain(initialValue);
    }

    return data;
  }

  read(initialValue: T) {
    this.data = this._read(initialValue);
  }

  write() {
    fs.writeFileSync(this.tempFilename, JSON.stringify(this.data.value()));
    fs.renameSync(this.tempFilename, this.filename);
  }
}

const db = new LodashJsonStorage<Data>(DB_PATH, { items: [] });

export default db;

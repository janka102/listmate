import axios from "axios";
import {
  GetServerSidePropsContext,
  GetServerSidePropsResult,
  NextApiRequest,
  NextApiResponse,
} from "next";
import { NextApiRequestCookies } from "next/dist/next-server/server/api-utils";

const { MEZZO_URL, LISTMATE_URL } = process.env;

export type MezzoUser = {
  username: string;
  timeZone: string;
  roles: string[];
  token: {
    username: string;
    userAgent: string;
    key: string;
    created: number;
    expires: number;
  };
};

export type GetServerSidePropsContextSession = GetServerSidePropsContext & {
  session: true | null;
};

export type GetServerSidePropsHandlerSession<P> = (
  context: GetServerSidePropsContextSession
) => GetServerSidePropsResult<P> | Promise<GetServerSidePropsResult<P>>;

export type NextApiRequestSession = NextApiRequest & {
  session: true | null;
};

export type NextApiHandlerSession<T> = (
  req: NextApiRequestSession,
  res: NextApiResponse<T>
) => void | Promise<void>;

async function useMezzo(req: { cookies: NextApiRequestCookies }) {
  const { token } = req.cookies;

  if (token) {
    try {
      const res = await axios(
        MEZZO_URL +
          "/api/verify/role/listmate?token=" +
          encodeURIComponent(token)
      );

      if (res.status === 200) {
        return true;
      }
    } catch (e) {
      console.log(e);
    }
  }

  return null;
}

export function withPageSession<P>(
  fn: GetServerSidePropsHandlerSession<P>
): GetServerSidePropsHandlerSession<P> {
  return async function (context) {
    const user = await useMezzo(context.req);

    if (!user) {
      return {
        redirect: {
          destination:
            MEZZO_URL +
            "/signin?url=" +
            encodeURIComponent(LISTMATE_URL + context.resolvedUrl),
          permanent: false,
        },
      };
    }

    context.session = user;

    return await fn(context);
  };
}

export function withApiSession<T>(
  fn: NextApiHandlerSession<T>
): NextApiHandlerSession<T> {
  return async function (req, res) {
    const user = await useMezzo(req);

    if (!user) {
      res.status(401).end();
      return;
    }

    req.session = user;

    return await fn(req, res);
  };
}

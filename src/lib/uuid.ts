export default function uuid() {
  return "xxxxxxxx-xxxx-4xxx-8xxx-xxxxxxxxxxxx".replace(/[x8]/g, (c) =>
    (c == "x"
      ? (Math.random() * 16) | 0
      : Number(c) | (Math.random() * 4)
    ).toString(16)
  );
}

import { MinusCircleIcon as TodoIcon } from "@heroicons/react/outline";
import { CheckCircleIcon as DoneIcon } from "@heroicons/react/solid";

export type CheckBoxProps = {
  size?: number;
  className?: string;
  checked: boolean;
  onChange?: (value: boolean) => void;
};

export default function CheckBox({
  size,
  className,
  checked,
  onChange,
}: CheckBoxProps) {
  const Icon = checked ? DoneIcon : TodoIcon;

  const iconClassName = size ? `h-${size} w-${size}` : "";
  className ??= "";

  return (
    <button
      className={
        "text-white bg-green-800 hover:bg-green-700 focus:bg-green-700 " +
        className
      }
      onClick={(e) => {
        e.preventDefault();

        if (onChange) onChange(!checked);
      }}
    >
      <Icon className={iconClassName} />
    </button>
  );
}

export type ListProps = {};

export default function List({ children }: React.PropsWithChildren<ListProps>) {
  return <ul className="flex flex-col divide-y divide-gray-700">{children}</ul>;
}

export type ListItemProps = {
  className?: string;
};

List.Item = function Item({
  className,
  children,
}: React.PropsWithChildren<ListItemProps>) {
  return <li className={className}>{children}</li>;
};

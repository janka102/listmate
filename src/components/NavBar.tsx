import Link from "next/link";
import { useRouter } from "next/router";

import Container from "./Container";

const linkStyle =
  "text-gray-300 hover:text-white border-transparent hover:border-green-800";
const activeLinkStyle = "text-white font-medium border-green-600";

function ActiveLink({
  href,
  children,
}: React.PropsWithChildren<{ href: string }>) {
  const router = useRouter();

  return (
    <Link href={href}>
      <a
        className={
          "px-3 py-2 border-b-2 hover:bg-green-800 focus:bg-green-800 " +
          (router.asPath === href ? activeLinkStyle : linkStyle)
        }
      >
        {children}
      </a>
    </Link>
  );
}

export default function NavBar() {
  return (
    <nav className="bg-green-900 text-gray-50">
      <Container>
        <div className="flex items-center">
          <ActiveLink href="/">Listmate</ActiveLink>
        </div>
      </Container>
    </nav>
  );
}

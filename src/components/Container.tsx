export type ContainerProps = {};

export default function Container({
  children,
}: React.PropsWithChildren<ContainerProps>) {
  return (
    <div className="container mx-auto px-4 sm:px-6 lg:px-8">{children}</div>
  );
}

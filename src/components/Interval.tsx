import { useEffect } from "react";

export type IntervalProps = {
  onTick: Function;
  ms: number;
  delay?: number;
};

export default function Interval({
  onTick,
  ms,
  delay = 0,
  children,
}: React.PropsWithChildren<IntervalProps>) {
  onTick();

  useEffect(() => {
    let interval: NodeJS.Timeout;

    const timeout = setTimeout(() => {
      interval = setInterval(() => {
        onTick();
      }, ms);
    }, delay);

    return () => {
      clearTimeout(timeout);
      clearInterval(interval);
    };
  });

  return <>{children}</>;
}

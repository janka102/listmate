import { PlusCircleIcon, XIcon } from "@heroicons/react/solid";
import { MutableRefObject, useEffect, useRef, useState } from "react";

export type TagListProps = {
  tags: string[];
  autocomplete?: string[];
  readonly?: boolean;
  onChange?: (tags: string[]) => void;
  className?: string;
};

type TagSearchProps = {
  autocomplete?: string[];
  onSubmit?: (tag: string | null) => void;
  parent?: MutableRefObject<HTMLElement | null>;
};

function TagSearch({ autocomplete = [], onSubmit, parent }: TagSearchProps) {
  const tagInput = useRef(null as HTMLInputElement | null);
  const rootEl = useRef(null as HTMLDivElement | null);
  const [tagText, setTagText] = useState("");
  const [displayList, setDisplayList] = useState([...autocomplete].sort());
  const [selected, setSelected] = useState(-1);

  useEffect(() => {
    if (tagInput.current) {
      tagInput.current.focus();
    }
  }, []);

  useEffect(() => {
    function onBlur(e: MouseEvent) {
      e.preventDefault();
      e.stopPropagation();

      for (
        let el: HTMLElement | null = e.target as HTMLElement;
        el && el !== document.documentElement;
        el = el.parentElement
      ) {
        if (el === rootEl.current || el === parent?.current) {
          return;
        }
      }

      if (onSubmit) onSubmit(null);
    }

    document.addEventListener("click", onBlur);

    return () => {
      document.removeEventListener("click", onBlur);
    };
  }, []);

  function changeText(text: string) {
    const newList = autocomplete
      .filter((tag) =>
        tag.trim().toLowerCase().includes(text.trim().toLowerCase())
      )
      .sort();
    setTagText(text);
    setSelected(Math.min(selected, displayList.length - 1));
    setDisplayList(newList);
  }

  return (
    <div className="absolute mt-2 w-full z-10 shadow-md" ref={rootEl}>
      <div className="flex">
        <input
          ref={tagInput}
          type="text"
          className={
            "px-2 py-1 flex-1 border-b-2 border-gray-900 focus:border-green-700 bg-gray-700 outline-none"
          }
          placeholder="Tag"
          value={tagText}
          onChange={(e) => changeText(e.target.value)}
          onKeyDown={(e) => {
            const text = tagText.trim();

            if (e.key === "Enter" || e.key === " ") {
              e.preventDefault();

              if (selected >= 0 && selected < displayList.length) {
                changeText(displayList[selected]);

                if (onSubmit) {
                  onSubmit(displayList[selected]);
                }
              } else {
                setTagText("");

                if (onSubmit) {
                  onSubmit(text);
                }
              }
            } else if (e.key === "ArrowDown") {
              e.preventDefault();
              setSelected(Math.min(selected + 1, displayList.length - 1));
            } else if (e.key === "ArrowUp") {
              e.preventDefault();
              setSelected(Math.max(selected - 1, -1));
            } else if (e.key === "Escape" && onSubmit) {
              changeText("");

              if (onSubmit) {
                onSubmit(null);
              }
            }
          }}
        />
        <button
          title="Close"
          className="h-8 bg-gray-700 hover:bg-red-600 focus:bg-red-600"
          onClick={() => onSubmit && onSubmit(null)}
        >
          <XIcon className="h-6 w-6 text-gray-300 hover:text-white" />
        </button>
      </div>

      <ul className="bg-gray-500 w-full max-h-72 overflow-y-auto divide-y divide-gray-400 shadow">
        {displayList.length ? (
          displayList.map((tag, i) => (
            <li key={tag}>
              <button
                className={`w-full px-2 py-1 text-left ${
                  selected === i ? "bg-gray-600" : ""
                }`}
                onMouseMove={() => {
                  if (selected !== i) {
                    setSelected(i);
                  }
                }}
                onClick={() => {
                  changeText("");

                  if (onSubmit) {
                    onSubmit(tag);
                  }
                }}
              >
                {tag}
              </button>
            </li>
          ))
        ) : autocomplete.length ? (
          <li>
            <i className="block w-full px-2 py-1 text-left">none</i>
          </li>
        ) : null}
      </ul>
    </div>
  );
}

export default function TagList({
  tags,
  autocomplete,
  readonly = false,
  onChange,
  className,
}: TagListProps) {
  const rootEl = useRef(null as HTMLDivElement | null);
  const [showAddTag, setShowAddTag] = useState(false);

  return (
    <div className={"relative " + className} ref={rootEl}>
      <ul
        className={`flex text-sm flex-wrap ${
          tags.length ? "-mx-1 -my-0.5" : ""
        }`}
      >
        {tags.map((tag, index) => (
          <li
            key={tag}
            className={`flex items-center p${
              readonly ? "x" : "r"
            }-1 mx-1 my-0.5 bg-gray-700`}
          >
            {readonly ? null : (
              <button
                className="h-full w-5 p-0.5 text-gray-300 hover:bg-red-600 hover:bg-opacity-75 hover:text-white focus:bg-red-600 focus:bg-opacity-75 focus:text-white"
                title="Remove tag"
                onClick={() => {
                  if (onChange) {
                    const newTags = [...tags];
                    newTags.splice(index, 1);
                    onChange(newTags);
                  }
                }}
              >
                <XIcon />
              </button>
            )}
            {tag}
          </li>
        ))}

        {readonly ? null : (
          <li className="flex items-center">
            <button title="Add tag" onClick={setShowAddTag.bind(null, true)}>
              <PlusCircleIcon className="h-6 w-6 text-gray-300 hover:text-white" />
            </button>
          </li>
        )}
      </ul>

      {showAddTag ? (
        <TagSearch
          autocomplete={autocomplete}
          onSubmit={(newTag) => {
            if (onChange && newTag) {
              onChange(tags.concat(newTag));
            } else if (!newTag) {
              setShowAddTag(false);
            }
          }}
        />
      ) : null}
    </div>
  );
}

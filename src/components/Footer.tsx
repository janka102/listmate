import { CodeIcon } from "@heroicons/react/outline";
import Container from "./Container";

export default function NavBar() {
  return (
    <footer className="bg-gray-800 text-gray-50">
      <Container>
        <div className="flex justify-between py-4">
          <span>&copy; {new Date().getFullYear()} Jesse Smick</span>
          <a
            className="hover:underline"
            href="https://gitlab.com/janka102/listmate"
          >
            <CodeIcon className="h-5 w-5 inline mr-1" />
            GitLab
          </a>
        </div>
      </Container>
    </footer>
  );
}
